#include <iostream>
//#include "/usr/include/c++/4.8/bits/stl_vector.h"
#include <vector>
#include <complex>
#include "../Eigen/Dense"
#include "../Eigen/Sparse"
#include "../Eigen/Core"
#include "../Eigen/SparseQR"

#include "quadratic.h"

using namespace std;
using namespace Eigen;

void solve_linear_system(double *sol, int **matrix, int *vector, int N){

	int i,j;

	MatrixXd A(N, N);

	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
			A(i,j) = (double) matrix[i][j];
		}
	}

	VectorXd b(N);
	for (i = 0; i < N; i++) {
		b(i) = vector[i];
	}

	VectorXd X = A.jacobiSvd(ComputeThinU | ComputeThinV).solve(b);

	for (i = 0; i < N; i++) {
		sol[i] = X.data()[i];
	}

}
